// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package tests

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
	"gitlab.com/alphaticks/abigen-starknet/accounts/abi"
	"gitlab.com/alphaticks/abigen-starknet/accounts/abi/bind"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// TestMetaData contains all meta data concerning the Test contract.
var TestMetaData = &bind.MetaData{
	ABI: "[{\"chain\":\"Starknet\",\"type\":\"-\"},{\"inputs\":[{\"name\":\"low\",\"offset\":0,\"type\":\"uint256\"},{\"name\":\"high\",\"offset\":1,\"type\":\"uint256\"}],\"name\":\"uint256\",\"size\":2,\"type\":\"struct\"},{\"inputs\":[{\"name\":\"from_\",\"type\":\"uint256\"},{\"name\":\"to\",\"type\":\"uint256\"},{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"keys\":[],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[{\"name\":\"owner\",\"type\":\"uint256\"},{\"name\":\"approved\",\"type\":\"uint256\"},{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"keys\":[],\"name\":\"Approval\",\"type\":\"event\"},{\"inputs\":[{\"name\":\"owner\",\"type\":\"uint256\"},{\"name\":\"operator\",\"type\":\"uint256\"},{\"name\":\"approved\",\"type\":\"uint256\"}],\"keys\":[],\"name\":\"ApprovalForAll\",\"type\":\"event\"},{\"inputs\":[{\"name\":\"previousOwner\",\"type\":\"uint256\"},{\"name\":\"newOwner\",\"type\":\"uint256\"}],\"keys\":[],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"inputs\":[{\"name\":\"name\",\"type\":\"uint256\"},{\"name\":\"symbol\",\"type\":\"uint256\"},{\"name\":\"owner\",\"type\":\"uint256\"}],\"name\":\"constructor\",\"outputs\":[],\"type\":\"constructor\"},{\"inputs\":[{\"name\":\"interfaceId\",\"type\":\"uint256\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"name\":\"success\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"name\":\"name\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"name\":\"symbol\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"owner\",\"type\":\"uint256\"}],\"name\":\"balanceOf\",\"outputs\":[{\"name\":\"balance\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"name\":\"owner\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getApproved\",\"outputs\":[{\"name\":\"approved\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"owner\",\"type\":\"uint256\"},{\"name\":\"operator\",\"type\":\"uint256\"}],\"name\":\"isApprovedForAll\",\"outputs\":[{\"name\":\"isApproved\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"name\":\"tokenURI\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"name\":\"owner\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"to\",\"type\":\"uint256\"},{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"operator\",\"type\":\"uint256\"},{\"name\":\"approved\",\"type\":\"uint256\"}],\"name\":\"setApprovalForAll\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"from_\",\"type\":\"uint256\"},{\"name\":\"to\",\"type\":\"uint256\"},{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"from_\",\"type\":\"uint256\"},{\"name\":\"to\",\"type\":\"uint256\"},{\"name\":\"tokenId\",\"type\":\"uint256\"},{\"name\":\"inputs_len\",\"type\":\"uint256\"},{\"name\":\"inputs\",\"type\":\"uint256*\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"to\",\"type\":\"uint256\"},{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"mint\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"burn\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"tokenId\",\"type\":\"uint256\"},{\"name\":\"tokenURI\",\"type\":\"uint256\"}],\"name\":\"setTokenURI\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[{\"name\":\"newOwner\",\"type\":\"uint256\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"type\":\"function\"}]",
}

// TestABI is the input ABI used to generate the binding from.
// Deprecated: Use TestMetaData.ABI instead.
var TestABI = TestMetaData.ABI

// Test is an auto generated Go binding around an Ethereum contract.
type Test struct {
	TestCaller     // Read-only binding to the contract
	TestTransactor // Write-only binding to the contract
	TestFilterer   // Log filterer for contract events
}

// TestCaller is an auto generated read-only Go binding around an Ethereum contract.
type TestCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TestTransactor is an auto generated write-only Go binding around an Ethereum contract.
type TestTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TestFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type TestFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TestSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type TestSession struct {
	Contract     *Test             // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// TestCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type TestCallerSession struct {
	Contract *TestCaller   // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// TestTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type TestTransactorSession struct {
	Contract     *TestTransactor   // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// TestRaw is an auto generated low-level Go binding around an Ethereum contract.
type TestRaw struct {
	Contract *Test // Generic contract binding to access the raw methods on
}

// TestCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type TestCallerRaw struct {
	Contract *TestCaller // Generic read-only contract binding to access the raw methods on
}

// TestTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type TestTransactorRaw struct {
	Contract *TestTransactor // Generic write-only contract binding to access the raw methods on
}

// NewTest creates a new instance of Test, bound to a specific deployed contract.
func NewTest(address common.Address, backend bind.ContractBackend) (*Test, error) {
	contract, err := bindTest(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Test{TestCaller: TestCaller{contract: contract}, TestTransactor: TestTransactor{contract: contract}, TestFilterer: TestFilterer{contract: contract}}, nil
}

// NewTestCaller creates a new read-only instance of Test, bound to a specific deployed contract.
func NewTestCaller(address common.Address, caller bind.ContractCaller) (*TestCaller, error) {
	contract, err := bindTest(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &TestCaller{contract: contract}, nil
}

// NewTestTransactor creates a new write-only instance of Test, bound to a specific deployed contract.
func NewTestTransactor(address common.Address, transactor bind.ContractTransactor) (*TestTransactor, error) {
	contract, err := bindTest(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &TestTransactor{contract: contract}, nil
}

// NewTestFilterer creates a new log filterer instance of Test, bound to a specific deployed contract.
func NewTestFilterer(address common.Address, filterer bind.ContractFilterer) (*TestFilterer, error) {
	contract, err := bindTest(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &TestFilterer{contract: contract}, nil
}

// bindTest binds a generic wrapper to an already deployed contract.
func bindTest(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(TestABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Test *TestRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Test.Contract.TestCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Test *TestRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Test.Contract.TestTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Test *TestRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Test.Contract.TestTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Test *TestCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Test.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Test *TestTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Test.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Test *TestTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Test.Contract.contract.Transact(opts, method, params...)
}

// BalanceOf is a free data retrieval call binding the contract method 0x9cc7f708.
//
// Solidity: function balanceOf(uint256 owner) view returns(uint256 balance)
func (_Test *TestCaller) BalanceOf(opts *bind.CallOpts, owner *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "balanceOf", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x9cc7f708.
//
// Solidity: function balanceOf(uint256 owner) view returns(uint256 balance)
func (_Test *TestSession) BalanceOf(owner *big.Int) (*big.Int, error) {
	return _Test.Contract.BalanceOf(&_Test.CallOpts, owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x9cc7f708.
//
// Solidity: function balanceOf(uint256 owner) view returns(uint256 balance)
func (_Test *TestCallerSession) BalanceOf(owner *big.Int) (*big.Int, error) {
	return _Test.Contract.BalanceOf(&_Test.CallOpts, owner)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(uint256 approved)
func (_Test *TestCaller) GetApproved(opts *bind.CallOpts, tokenId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "getApproved", tokenId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(uint256 approved)
func (_Test *TestSession) GetApproved(tokenId *big.Int) (*big.Int, error) {
	return _Test.Contract.GetApproved(&_Test.CallOpts, tokenId)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(uint256 approved)
func (_Test *TestCallerSession) GetApproved(tokenId *big.Int) (*big.Int, error) {
	return _Test.Contract.GetApproved(&_Test.CallOpts, tokenId)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0x8d9188d5.
//
// Solidity: function isApprovedForAll(uint256 owner, uint256 operator) view returns(uint256 isApproved)
func (_Test *TestCaller) IsApprovedForAll(opts *bind.CallOpts, owner *big.Int, operator *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "isApprovedForAll", owner, operator)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0x8d9188d5.
//
// Solidity: function isApprovedForAll(uint256 owner, uint256 operator) view returns(uint256 isApproved)
func (_Test *TestSession) IsApprovedForAll(owner *big.Int, operator *big.Int) (*big.Int, error) {
	return _Test.Contract.IsApprovedForAll(&_Test.CallOpts, owner, operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0x8d9188d5.
//
// Solidity: function isApprovedForAll(uint256 owner, uint256 operator) view returns(uint256 isApproved)
func (_Test *TestCallerSession) IsApprovedForAll(owner *big.Int, operator *big.Int) (*big.Int, error) {
	return _Test.Contract.IsApprovedForAll(&_Test.CallOpts, owner, operator)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(uint256 name)
func (_Test *TestCaller) Name(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(uint256 name)
func (_Test *TestSession) Name() (*big.Int, error) {
	return _Test.Contract.Name(&_Test.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(uint256 name)
func (_Test *TestCallerSession) Name() (*big.Int, error) {
	return _Test.Contract.Name(&_Test.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(uint256 owner)
func (_Test *TestCaller) Owner(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(uint256 owner)
func (_Test *TestSession) Owner() (*big.Int, error) {
	return _Test.Contract.Owner(&_Test.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(uint256 owner)
func (_Test *TestCallerSession) Owner() (*big.Int, error) {
	return _Test.Contract.Owner(&_Test.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(uint256 owner)
func (_Test *TestCaller) OwnerOf(opts *bind.CallOpts, tokenId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "ownerOf", tokenId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(uint256 owner)
func (_Test *TestSession) OwnerOf(tokenId *big.Int) (*big.Int, error) {
	return _Test.Contract.OwnerOf(&_Test.CallOpts, tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(uint256 owner)
func (_Test *TestCallerSession) OwnerOf(tokenId *big.Int) (*big.Int, error) {
	return _Test.Contract.OwnerOf(&_Test.CallOpts, tokenId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x84d1da34.
//
// Solidity: function supportsInterface(uint256 interfaceId) view returns(uint256 success)
func (_Test *TestCaller) SupportsInterface(opts *bind.CallOpts, interfaceId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x84d1da34.
//
// Solidity: function supportsInterface(uint256 interfaceId) view returns(uint256 success)
func (_Test *TestSession) SupportsInterface(interfaceId *big.Int) (*big.Int, error) {
	return _Test.Contract.SupportsInterface(&_Test.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x84d1da34.
//
// Solidity: function supportsInterface(uint256 interfaceId) view returns(uint256 success)
func (_Test *TestCallerSession) SupportsInterface(interfaceId *big.Int) (*big.Int, error) {
	return _Test.Contract.SupportsInterface(&_Test.CallOpts, interfaceId)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(uint256 symbol)
func (_Test *TestCaller) Symbol(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(uint256 symbol)
func (_Test *TestSession) Symbol() (*big.Int, error) {
	return _Test.Contract.Symbol(&_Test.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(uint256 symbol)
func (_Test *TestCallerSession) Symbol() (*big.Int, error) {
	return _Test.Contract.Symbol(&_Test.CallOpts)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(uint256 tokenURI)
func (_Test *TestCaller) TokenURI(opts *bind.CallOpts, tokenId *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _Test.contract.Call(opts, &out, "tokenURI", tokenId)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(uint256 tokenURI)
func (_Test *TestSession) TokenURI(tokenId *big.Int) (*big.Int, error) {
	return _Test.Contract.TokenURI(&_Test.CallOpts, tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(uint256 tokenURI)
func (_Test *TestCallerSession) TokenURI(tokenId *big.Int) (*big.Int, error) {
	return _Test.Contract.TokenURI(&_Test.CallOpts, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x5d35a3d9.
//
// Solidity: function approve(uint256 to, uint256 tokenId) returns()
func (_Test *TestTransactor) Approve(opts *bind.TransactOpts, to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "approve", to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x5d35a3d9.
//
// Solidity: function approve(uint256 to, uint256 tokenId) returns()
func (_Test *TestSession) Approve(to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.Approve(&_Test.TransactOpts, to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x5d35a3d9.
//
// Solidity: function approve(uint256 to, uint256 tokenId) returns()
func (_Test *TestTransactorSession) Approve(to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.Approve(&_Test.TransactOpts, to, tokenId)
}

// Burn is a paid mutator transaction binding the contract method 0x42966c68.
//
// Solidity: function burn(uint256 tokenId) returns()
func (_Test *TestTransactor) Burn(opts *bind.TransactOpts, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "burn", tokenId)
}

// Burn is a paid mutator transaction binding the contract method 0x42966c68.
//
// Solidity: function burn(uint256 tokenId) returns()
func (_Test *TestSession) Burn(tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.Burn(&_Test.TransactOpts, tokenId)
}

// Burn is a paid mutator transaction binding the contract method 0x42966c68.
//
// Solidity: function burn(uint256 tokenId) returns()
func (_Test *TestTransactorSession) Burn(tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.Burn(&_Test.TransactOpts, tokenId)
}

// Mint is a paid mutator transaction binding the contract method 0x1b2ef1ca.
//
// Solidity: function mint(uint256 to, uint256 tokenId) returns()
func (_Test *TestTransactor) Mint(opts *bind.TransactOpts, to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "mint", to, tokenId)
}

// Mint is a paid mutator transaction binding the contract method 0x1b2ef1ca.
//
// Solidity: function mint(uint256 to, uint256 tokenId) returns()
func (_Test *TestSession) Mint(to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.Mint(&_Test.TransactOpts, to, tokenId)
}

// Mint is a paid mutator transaction binding the contract method 0x1b2ef1ca.
//
// Solidity: function mint(uint256 to, uint256 tokenId) returns()
func (_Test *TestTransactorSession) Mint(to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.Mint(&_Test.TransactOpts, to, tokenId)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Test *TestTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Test *TestSession) RenounceOwnership() (*types.Transaction, error) {
	return _Test.Contract.RenounceOwnership(&_Test.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Test *TestTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Test.Contract.RenounceOwnership(&_Test.TransactOpts)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x4b1689f5.
//
// Solidity: function safeTransferFrom(uint256 from_, uint256 to, uint256 tokenId, uint256 inputs_len, uint256* inputs) returns()
func (_Test *TestTransactor) SafeTransferFrom(opts *bind.TransactOpts, from_ *big.Int, to *big.Int, tokenId *big.Int, inputs_len *big.Int, inputs *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "safeTransferFrom", from_, to, tokenId, inputs_len, inputs)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x4b1689f5.
//
// Solidity: function safeTransferFrom(uint256 from_, uint256 to, uint256 tokenId, uint256 inputs_len, uint256* inputs) returns()
func (_Test *TestSession) SafeTransferFrom(from_ *big.Int, to *big.Int, tokenId *big.Int, inputs_len *big.Int, inputs *big.Int) (*types.Transaction, error) {
	return _Test.Contract.SafeTransferFrom(&_Test.TransactOpts, from_, to, tokenId, inputs_len, inputs)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0x4b1689f5.
//
// Solidity: function safeTransferFrom(uint256 from_, uint256 to, uint256 tokenId, uint256 inputs_len, uint256* inputs) returns()
func (_Test *TestTransactorSession) SafeTransferFrom(from_ *big.Int, to *big.Int, tokenId *big.Int, inputs_len *big.Int, inputs *big.Int) (*types.Transaction, error) {
	return _Test.Contract.SafeTransferFrom(&_Test.TransactOpts, from_, to, tokenId, inputs_len, inputs)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xd203da9e.
//
// Solidity: function setApprovalForAll(uint256 operator, uint256 approved) returns()
func (_Test *TestTransactor) SetApprovalForAll(opts *bind.TransactOpts, operator *big.Int, approved *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "setApprovalForAll", operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xd203da9e.
//
// Solidity: function setApprovalForAll(uint256 operator, uint256 approved) returns()
func (_Test *TestSession) SetApprovalForAll(operator *big.Int, approved *big.Int) (*types.Transaction, error) {
	return _Test.Contract.SetApprovalForAll(&_Test.TransactOpts, operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xd203da9e.
//
// Solidity: function setApprovalForAll(uint256 operator, uint256 approved) returns()
func (_Test *TestTransactorSession) SetApprovalForAll(operator *big.Int, approved *big.Int) (*types.Transaction, error) {
	return _Test.Contract.SetApprovalForAll(&_Test.TransactOpts, operator, approved)
}

// SetTokenURI is a paid mutator transaction binding the contract method 0xd7823360.
//
// Solidity: function setTokenURI(uint256 tokenId, uint256 tokenURI) returns()
func (_Test *TestTransactor) SetTokenURI(opts *bind.TransactOpts, tokenId *big.Int, tokenURI *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "setTokenURI", tokenId, tokenURI)
}

// SetTokenURI is a paid mutator transaction binding the contract method 0xd7823360.
//
// Solidity: function setTokenURI(uint256 tokenId, uint256 tokenURI) returns()
func (_Test *TestSession) SetTokenURI(tokenId *big.Int, tokenURI *big.Int) (*types.Transaction, error) {
	return _Test.Contract.SetTokenURI(&_Test.TransactOpts, tokenId, tokenURI)
}

// SetTokenURI is a paid mutator transaction binding the contract method 0xd7823360.
//
// Solidity: function setTokenURI(uint256 tokenId, uint256 tokenURI) returns()
func (_Test *TestTransactorSession) SetTokenURI(tokenId *big.Int, tokenURI *big.Int) (*types.Transaction, error) {
	return _Test.Contract.SetTokenURI(&_Test.TransactOpts, tokenId, tokenURI)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x310ed7f0.
//
// Solidity: function transferFrom(uint256 from_, uint256 to, uint256 tokenId) returns()
func (_Test *TestTransactor) TransferFrom(opts *bind.TransactOpts, from_ *big.Int, to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "transferFrom", from_, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x310ed7f0.
//
// Solidity: function transferFrom(uint256 from_, uint256 to, uint256 tokenId) returns()
func (_Test *TestSession) TransferFrom(from_ *big.Int, to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.TransferFrom(&_Test.TransactOpts, from_, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x310ed7f0.
//
// Solidity: function transferFrom(uint256 from_, uint256 to, uint256 tokenId) returns()
func (_Test *TestTransactorSession) TransferFrom(from_ *big.Int, to *big.Int, tokenId *big.Int) (*types.Transaction, error) {
	return _Test.Contract.TransferFrom(&_Test.TransactOpts, from_, to, tokenId)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xd23e8489.
//
// Solidity: function transferOwnership(uint256 newOwner) returns()
func (_Test *TestTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner *big.Int) (*types.Transaction, error) {
	return _Test.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xd23e8489.
//
// Solidity: function transferOwnership(uint256 newOwner) returns()
func (_Test *TestSession) TransferOwnership(newOwner *big.Int) (*types.Transaction, error) {
	return _Test.Contract.TransferOwnership(&_Test.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xd23e8489.
//
// Solidity: function transferOwnership(uint256 newOwner) returns()
func (_Test *TestTransactorSession) TransferOwnership(newOwner *big.Int) (*types.Transaction, error) {
	return _Test.Contract.TransferOwnership(&_Test.TransactOpts, newOwner)
}

// TestApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the Test contract.
type TestApprovalIterator struct {
	Event *TestApproval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TestApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TestApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TestApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TestApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TestApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TestApproval represents a Approval event raised by the Test contract.
type TestApproval struct {
	Owner    *big.Int
	Approved *big.Int
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x0134692b230b9e1ffa39098904722134159652b09c5bc41d88d6698779d228ff.
//
// Solidity: event Approval(uint256 owner, uint256 approved, uint256 tokenId)
func (_Test *TestFilterer) FilterApproval(opts *bind.FilterOpts) (*TestApprovalIterator, error) {

	logs, sub, err := _Test.contract.FilterLogs(opts, "Approval")
	if err != nil {
		return nil, err
	}
	return &TestApprovalIterator{contract: _Test.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x0134692b230b9e1ffa39098904722134159652b09c5bc41d88d6698779d228ff.
//
// Solidity: event Approval(uint256 owner, uint256 approved, uint256 tokenId)
func (_Test *TestFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *TestApproval) (event.Subscription, error) {

	logs, sub, err := _Test.contract.WatchLogs(opts, "Approval")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TestApproval)
				if err := _Test.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x0134692b230b9e1ffa39098904722134159652b09c5bc41d88d6698779d228ff.
//
// Solidity: event Approval(uint256 owner, uint256 approved, uint256 tokenId)
func (_Test *TestFilterer) ParseApproval(log types.Log) (*TestApproval, error) {
	event := new(TestApproval)
	if err := _Test.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TestApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the Test contract.
type TestApprovalForAllIterator struct {
	Event *TestApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TestApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TestApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TestApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TestApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TestApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TestApprovalForAll represents a ApprovalForAll event raised by the Test contract.
type TestApprovalForAll struct {
	Owner    *big.Int
	Operator *big.Int
	Approved *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x0006ad9ed7b6318f1bcffefe19df9aeb40d22c36bed567e1925a5ccde0536edd.
//
// Solidity: event ApprovalForAll(uint256 owner, uint256 operator, uint256 approved)
func (_Test *TestFilterer) FilterApprovalForAll(opts *bind.FilterOpts) (*TestApprovalForAllIterator, error) {

	logs, sub, err := _Test.contract.FilterLogs(opts, "ApprovalForAll")
	if err != nil {
		return nil, err
	}
	return &TestApprovalForAllIterator{contract: _Test.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x0006ad9ed7b6318f1bcffefe19df9aeb40d22c36bed567e1925a5ccde0536edd.
//
// Solidity: event ApprovalForAll(uint256 owner, uint256 operator, uint256 approved)
func (_Test *TestFilterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *TestApprovalForAll) (event.Subscription, error) {

	logs, sub, err := _Test.contract.WatchLogs(opts, "ApprovalForAll")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TestApprovalForAll)
				if err := _Test.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x0006ad9ed7b6318f1bcffefe19df9aeb40d22c36bed567e1925a5ccde0536edd.
//
// Solidity: event ApprovalForAll(uint256 owner, uint256 operator, uint256 approved)
func (_Test *TestFilterer) ParseApprovalForAll(log types.Log) (*TestApprovalForAll, error) {
	event := new(TestApprovalForAll)
	if err := _Test.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TestOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Test contract.
type TestOwnershipTransferredIterator struct {
	Event *TestOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TestOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TestOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TestOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TestOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TestOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TestOwnershipTransferred represents a OwnershipTransferred event raised by the Test contract.
type TestOwnershipTransferred struct {
	PreviousOwner *big.Int
	NewOwner      *big.Int
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x01390fd803c110ac71730ece1decfc34eb1d0088e295d4f1b125dda1e0c5b9ff.
//
// Solidity: event OwnershipTransferred(uint256 previousOwner, uint256 newOwner)
func (_Test *TestFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts) (*TestOwnershipTransferredIterator, error) {

	logs, sub, err := _Test.contract.FilterLogs(opts, "OwnershipTransferred")
	if err != nil {
		return nil, err
	}
	return &TestOwnershipTransferredIterator{contract: _Test.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x01390fd803c110ac71730ece1decfc34eb1d0088e295d4f1b125dda1e0c5b9ff.
//
// Solidity: event OwnershipTransferred(uint256 previousOwner, uint256 newOwner)
func (_Test *TestFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *TestOwnershipTransferred) (event.Subscription, error) {

	logs, sub, err := _Test.contract.WatchLogs(opts, "OwnershipTransferred")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TestOwnershipTransferred)
				if err := _Test.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x01390fd803c110ac71730ece1decfc34eb1d0088e295d4f1b125dda1e0c5b9ff.
//
// Solidity: event OwnershipTransferred(uint256 previousOwner, uint256 newOwner)
func (_Test *TestFilterer) ParseOwnershipTransferred(log types.Log) (*TestOwnershipTransferred, error) {
	event := new(TestOwnershipTransferred)
	if err := _Test.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// TestTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the Test contract.
type TestTransferIterator struct {
	Event *TestTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TestTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TestTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TestTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TestTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TestTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TestTransfer represents a Transfer event raised by the Test contract.
type TestTransfer struct {
	From    *big.Int
	To      *big.Int
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0x0099cd8bde557814842a3121e8ddfd433a539b8c9f14bf31ebf108d12e6196e9.
//
// Solidity: event Transfer(uint256 from_, uint256 to, uint256 tokenId)
func (_Test *TestFilterer) FilterTransfer(opts *bind.FilterOpts) (*TestTransferIterator, error) {

	logs, sub, err := _Test.contract.FilterLogs(opts, "Transfer")
	if err != nil {
		return nil, err
	}
	return &TestTransferIterator{contract: _Test.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0x0099cd8bde557814842a3121e8ddfd433a539b8c9f14bf31ebf108d12e6196e9.
//
// Solidity: event Transfer(uint256 from_, uint256 to, uint256 tokenId)
func (_Test *TestFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *TestTransfer) (event.Subscription, error) {

	logs, sub, err := _Test.contract.WatchLogs(opts, "Transfer")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TestTransfer)
				if err := _Test.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0x0099cd8bde557814842a3121e8ddfd433a539b8c9f14bf31ebf108d12e6196e9.
//
// Solidity: event Transfer(uint256 from_, uint256 to, uint256 tokenId)
func (_Test *TestFilterer) ParseTransfer(log types.Log) (*TestTransfer, error) {
	event := new(TestTransfer)
	if err := _Test.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
