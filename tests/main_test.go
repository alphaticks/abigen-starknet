package tests

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"math/big"
	"testing"
)

func TestHashing(t *testing.T) {
	abi, err := TestMetaData.GetAbi()
	if err != nil {
		t.Fatal(err)
	}
	for name := range abi.Events {
		calc := GetSelectorFromName(name)
		h := common.BigToHash(calc)
		if h.String() != abi.Events[name].ID.String() {
			t.Fatalf("expected %s, got %s", h.String(), abi.Events[name].ID.String())
		}
	}
}

func GetSelectorFromName(funcName string) *big.Int {
	kec := crypto.Keccak256([]byte(funcName))

	maskedKec := maskBits(250, 8, kec)

	return new(big.Int).SetBytes(maskedKec)
}

// mask excess bits
func maskBits(mask, wordSize int, slice []byte) (ret []byte) {
	excess := len(slice)*wordSize - mask
	for _, by := range slice {
		if excess > 0 {
			if excess > wordSize {
				excess = excess - wordSize
				continue
			}
			by <<= excess
			by >>= excess
			excess = 0
		}
		ret = append(ret, by)
	}
	return ret
}
